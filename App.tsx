import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import HomeScreen from "./screens/HomeScreen";
import CameraScreen from "./screens/CameraScreen";
import OneCategoryScreen from "./screens/OneCategoryScreen";
import ListRecettesScreen from "./screens/ListRecettesScreen";
import RecetteScreen from "./screens/RecetteScreen";


export default function App() {

    const Stack = createNativeStackNavigator();

    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home" component={HomeScreen} />
                <Stack.Screen name="Camera" component={CameraScreen} />
                <Stack.Screen name="OneCategory" component={OneCategoryScreen} />
                <Stack.Screen name="OneRecette" component={RecetteScreen} />
                <Stack.Screen name="ListRecettes" component={ListRecettesScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}