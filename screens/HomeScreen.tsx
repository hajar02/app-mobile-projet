import React, { useEffect, useState } from 'react';
import { StyleSheet, Dimensions, ScrollView } from 'react-native';
import { Category } from '../entities';
import { fetchAllCategory } from '../recettes-service';
import ItemCategory from '../components/ItemCategory';
import { useNavigation } from '@react-navigation/native';


const { width } = Dimensions.get('window');

export default function Categories() {
  const [categories, setCategories] = useState<Category[]>([]);
  const navigation = useNavigation();

  useEffect(() => {
    fetchAllCategory().then(data => {
      setCategories(data);
    });
  }, []);

  return (
    <ScrollView contentContainerStyle={styles.container}>
      {categories.map(category => (
        <ItemCategory
          key={category.idCategory}
          category={category}
          navigation={navigation}
        />
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
        container: {
          paddingHorizontal: 5,
          paddingTop: 60,
          paddingBottom: 60,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fff',
        },
      });
      
