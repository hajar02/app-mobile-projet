import { Button, Image, Text, View } from "react-native";
import * as ImagePicker from "expo-image-picker";
import { DeviceMotion, DeviceMotionMeasurement } from 'expo-sensors';
import { useEffect, useState } from "react";

export default function CameraScreen() {
  const [picked, setPicked] = useState('');
  const [data, setData] = useState<DeviceMotionMeasurement>();

  async function pickImage(camera = true) {
    let result;
    if (camera) {
      result = await ImagePicker.launchCameraAsync();
    } else {
      result = await ImagePicker.launchImageLibraryAsync();
    }
    if (result.assets?.length) {
      setPicked(result.assets[0].uri);
    }
  }

  useEffect(() => {
    let subscription: any;

    async function startSensor() {
      await DeviceMotion.requestPermissionsAsync();
      DeviceMotion.setUpdateInterval(50);
      subscription = DeviceMotion.addListener(setData);
    }

    startSensor();
    return () => subscription?.remove();
  }, []);

  const { accelerationIncludingGravity } = data ?? {};
  const { x: translateX = 0, y: translateY = 0 } = accelerationIncludingGravity ?? {};
  
  return (
    <View style={{ flex: 1 }}>
      <Text>{translateX} {translateY}</Text>
      <Button title="take picture" onPress={() => pickImage()} />
      <Button title="choose picture" onPress={() => pickImage(false)} />
      {picked && (
        <Image
          source={{ uri: picked }}
          style={{
            width: 250,
            height: 250,
            transform: [
              { perspective: 1 },
              { translateX: translateX*20 },
              { translateY: -translateY*20 }, 
            ],
          }}
        />
      )}
    </View>
  );
  
  
}
