/*import { StatusBar } from 'expo-status-bar';
import { Button, StyleSheet, View } from 'react-native';
import Counter from '../components/Counter';
import { useNavigation } from '@react-navigation/native';

export default function HomeScreen() {
  const navigation = useNavigation<any>();

  return (
    <View style={styles.container}>
      <Counter />
      <Button title="Go to camera" onPress={() => navigation.navigate("Camera")} />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});*/


//import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, Dimensions, FlatList } from 'react-native';
import { fetchAllCategory } from '../recettes-service';
import { Category } from '../entities';

const { width } = Dimensions.get('window');

export default function HomeScreen() {
  const [categories, setCategories] = useState<any[]>([]);

  useEffect(() => {
    async function fetchData() {
      const data: Category[] = await fetchAllCategory();
      setCategories(data);
    }
    fetchData();
  }, []);

  const renderItem = ({ item }: { item: Category }) => {
    return (
      <View style={styles.categoryContainer}>
        <Image source={{ uri: item.strCategoryThumb }} style={styles.categoryImage} />
        <Text style={styles.categoryName}>{item.strCategory}</Text>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={categories}
        renderItem={renderItem}
        keyExtractor={(item: Category) => item.strCategory}
        numColumns={2}
        columnWrapperStyle={styles.row}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    justifyContent: 'space-around',
  },
  categoryContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 200,
    width: width / 2 - 30,
    backgroundColor: '#eee',
    borderRadius: 10,
    margin: 10,
  },
  categoryImage: {
    height: 150,
    width: 150,
    borderRadius: 75,
    borderWidth: 1,
    borderColor: '#ddd',
  },
  categoryName: {
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 10,
    textAlign: 'center',
  },
});
