/*import React, { useEffect, useState } from 'react';
import { StyleSheet, Dimensions, ScrollView } from 'react-native';
import { Category, Recette } from '../entities';
import { fetchOneCategory } from '../recettes-service';
import ItemRecette from '../components/ItemRecette';
import { useRoute } from '@react-navigation/native';

export default function OneCategoryScreen() {
  const [recettes, setRecettes] = useState<Recette[]>([]);
  const [category, setCategory] = useState<Category[]>([]);
  const { width } = Dimensions.get('window');
  const route = useRoute<any>();
  const { idCategory } = route.params; 

  useEffect(() => {
    if (!idCategory) {
        return;
    }
    fetchOneCategory(Number(idCategory))
        .then(data => setCategory(data))
        .catch(error => {
            console.log(error);
            if (error.response.status == 404) {
                route.push('/404');
            }
        });

}, [idCategory]);


  return (
    <ScrollView contentContainerStyle={styles.container}>
      {recettes.map(recette => (
        <ItemRecette
          key={recette.idMeal}
          recette={recette}
        />
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
        container: {
          paddingHorizontal: 5,
          paddingTop: 60,
          paddingBottom: 60,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fff',
        },
      });*/

import React, { useEffect, useState } from 'react';
import { StyleSheet, Dimensions, ScrollView } from 'react-native';
import { Category, Recette } from '../entities';
import { fetchRecetteCategory, fetchRecipesByCategory } from '../recettes-service';
import ItemRecette from '../components/ItemRecette';
import { useRoute, useNavigation } from '@react-navigation/native';

export default function OneCategoryScreen() {
  const [recettes, setRecettes] = useState<Recette[]>([]);
  const [category, setCategory] = useState<Category>();

  const { width } = Dimensions.get('window');
  const route = useRoute<any>();
  const { strCategory } = route.params;
  const navigation = useNavigation();

  useEffect(() => {
   
    fetchRecetteCategory(strCategory)
      .then((data) => setRecettes(data))
      .catch((error) => {
        console.log(error);
        if (error.response.status == 404) {
          route.push('/404');
        }
      });
  }, [strCategory, navigation]);

  console.log(recettes);

  return (
    <ScrollView contentContainerStyle={styles.container}>
      {recettes.map((recette) => (
        <ItemRecette key={recette?.idMeal} recette={recette} navigation={navigation}  />
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 5,
    paddingTop: 60,
    paddingBottom: 60,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});
