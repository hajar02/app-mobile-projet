import React, { useEffect, useState } from 'react';
import { StyleSheet, Dimensions, ScrollView, Image, Text, View } from 'react-native';
import { Recette } from '../entities';
import { fetchOneRecette } from '../recettes-service';
import ItemRecette from '../components/ItemRecette';
import { useRoute, useNavigation } from '@react-navigation/native';

interface Props {
  recette: Recette;
  navigation: any;
}

export default function RecetteScreen() {
  const [recette, setRecette] = useState<Recette>();

  const { width } = Dimensions.get('window');
  const route = useRoute<any>();
  const { idMeal } = route.params;
  const navigation = useNavigation();

  useEffect(() => {
    fetchOneRecette(idMeal)
      .then((data) => setRecette(data))
      .catch((error) => {
        console.log(error);
        if (error.response.status == 404) {
          route.push('/404');
        }
      });
  }, [idMeal, navigation]);

  console.log(recette);

  return (
    <ScrollView contentContainerStyle={styles.container}>
      {recette && (
        <View style={styles.detailsContainer}>
          <View style={styles.imageContainer}>
            <Image style={styles.image} source={{ uri: recette.strMealThumb }} />
          </View>
          <Text style={styles.title}>{recette.strMeal}</Text>
          <View style={styles.infoContainer}>
            <Text style={styles.infoLabel}>Category:</Text>
            <Text style={styles.info}>{recette.strCategory}</Text>
          </View>
          <View style={styles.infoContainer}>
            <Text style={styles.infoLabel}>Area:</Text>
            <Text style={styles.info}>{recette.strArea}</Text>
          </View>
          {/* <View style={styles.ingredientsContainer}>
            <Text style={styles.ingredientsTitle}>Ingredients:</Text>
            <View style={styles.ingredientsList}>
              {recette.ingredients.map((ingredient, index) => {
                if (!ingredient) {
                  return null;
                }
                return (
                  <View style={styles.ingredientContainer} key={index}>
                    <Text style={styles.ingredient}>{ingredient}</Text>
                    <Text style={styles.measure}>{recette.measures[index]}</Text>
                  </View>
                );
              })}
            </View>
          </View> */}
          <View style={styles.instructionsContainer}>
            <Text style={styles.instructionsTitle}>Instructions:</Text>
            <Text style={styles.instructions}>{recette.strInstructions}</Text>
          </View>
        </View>
      )}
    </ScrollView>
  );
  
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: '#fff',
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  imageContainer: {
    alignItems: 'center',
    marginBottom: 20,
  },
  image: {
    width: Dimensions.get('window').width - 40,
    height: Dimensions.get('window').width - 40,
    borderRadius: 10,
  },
  detailsContainer: {},
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  infoContainer: {
    flexDirection: 'row',
    marginBottom: 5,
  },
  infoLabel: {
    fontWeight: 'bold',
    marginRight: 5,
  },
  info: {
    flex: 1,
  },
  ingredientsContainer: {
    marginTop: 20,
    marginBottom: 10,
  },
  ingredientsTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  ingredientsList: {},
  ingredientContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
    },
    ingredient: {
    flex: 1,
    },
    measure: {
    fontWeight: 'bold',
    },
    instructionsContainer: {
    marginTop: 20,
    marginBottom: 10,
    },
    instructionsTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
    },
    instructions: {},
    });