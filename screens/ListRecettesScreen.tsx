import React, { useEffect, useState } from 'react';
import { StyleSheet, Dimensions, ScrollView } from 'react-native';
import { Recette } from '../entities';
import { fetchAllRecette } from '../recettes-service';
import ItemRecette from '../components/ItemRecette';
import { useNavigation } from '@react-navigation/native';

const { width } = Dimensions.get('window');

export default function ListRecettesScreen() {
  const [recettes, setRecettes] = useState<Recette[]>([]);
  const navigation = useNavigation();

  useEffect(() => {
    fetchAllRecette().then(data => {
      setRecettes(data);
    });
  }, []);
  
  return (
    <ScrollView contentContainerStyle={styles.container}>
      {recettes.map(recette => 
        <ItemRecette
          key={recette.idMeal}
          recette={recette}
       
        />
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
        container: {
          paddingHorizontal: 5,
          paddingTop: 60,
          paddingBottom: 60,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fff',
        },
      });
      
