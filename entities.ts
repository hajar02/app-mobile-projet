export interface Recette {
    idMeal:number;
    strMeal: string;
    strCategory: string;
    strArea: string;
    strInstructions: string;
    strMealThumb: string;
    strYoutube: string;
    strIngredient1: string;
    strIngredient2: string;
    strIngredient3: string;
    strIngredient4: string;
    strIngredient5: string;
    strIngredient6: string;
    strMeasure1: string;
    strMeasure2: string;
    strMeasure3: string;
    strMeasure4: string;
    ingredients: string[];
    measures: string[];
}

export interface Category {
    idCategory?:number;
    strCategory:string;
    strCategoryThumb: string;
    strCategoryDescription: string;
    recettes: Recette [];
}

