import React from 'react';
import { Recette } from '../entities';
import { StyleSheet, View, ImageBackground, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native'; 

interface Props {
  recette: Recette;
  navigation: any;
}

const ItemRecette = ({ recette, navigation }: Props) => {
  const router = useNavigation<any>();

  function OneRecette(idMeal: number) {
    router.navigate('OneRecette', { idMeal: idMeal});
  }

  console.log(recette);

  return (
    <TouchableOpacity onPress={() => OneRecette(recette.idMeal)}
    style={styles.card} >
      <ImageBackground
        style={styles.image}
        source={{ uri: recette.strMealThumb }}
        blurRadius={1}
      >
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{recette.strMeal}</Text>
          <View style={styles.categoryAreaContainer}>
            <Text style={styles.category}>{recette.strCategory}</Text>
            <Text style={styles.area}>{recette.strArea}</Text>
          </View>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: {
    width: '90%',
    borderRadius: 20,
    overflow: 'hidden',
    marginVertical: 15,
    marginHorizontal: 'auto',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderWidth: 1,
    borderColor: '#ddd',
  },
  image: {
    height: 230,
    resizeMode: 'cover',
    justifyContent: 'flex-end',
  },
  titleContainer: {
    backgroundColor: 'rgba(0,0,0,0.6)',
    paddingVertical: 16,
    paddingHorizontal: 24,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  title: {
    color: '#fff',
    fontSize: 28,
    fontWeight: 'bold',
    textShadowColor: '#000',
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 5,
    marginBottom: 8,
  },
  categoryAreaContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 8,
  },
  category: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 16,
    textTransform: 'uppercase',
    backgroundColor: '#f44336',
    paddingHorizontal: 12,
    paddingVertical: 4,
    borderRadius: 15,
  },
  area: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 16,
    textTransform: 'uppercase',
    backgroundColor: '#4caf50',
    paddingHorizontal: 12,
    paddingVertical: 4,
    borderRadius: 15,
  },
});

export default ItemRecette;
