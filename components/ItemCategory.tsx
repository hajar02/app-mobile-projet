import React from 'react';
import { Category } from '../entities';
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

interface Props {
  category: Category;
  navigation: any;
}

const ItemCategory = ({ category, navigation }: Props) => {
  const router = useNavigation<any>();

  function OneCategory(strCategory: string) {
    router.navigate('OneCategory', { strCategory: strCategory});
  }

  return (
    <TouchableOpacity onPress={() => OneCategory(category.strCategory)}
    style={styles.container} >
        <ImageBackground
          source={{ uri: category.strCategoryThumb }}
          style={styles.image}
        >
          <Text style={styles.text}>{category.strCategory}</Text>
        </ImageBackground>
     </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '40%',
    aspectRatio: 1,
    overflow: 'hidden',
    margin: 10,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1.5,
    borderColor: 'black',
    borderRadius: 20,
    backgroundColor: '#0E0E0E'
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
    textShadowColor: 'rgba(0, 0, 0, 0.85)',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
});

export default ItemCategory;

//<TouchableOpacity onPress={() => navigation.navigate("OneCategoryScreen")}>