import axios from "axios";
import { Category, Recette } from "./entities";

export async function fetchRecetteCategory(strCategory: string) {
  const response = await axios.get<{meals:Recette[]}>( `https://www.themealdb.com/api/json/v1/1/filter.php?c=${strCategory}`);
  return response.data.meals;
}

export async function fetchAllRecette() {
    const response = await axios.get<{meals:Recette[]}>('https://www.themealdb.com/api/json/v1/1/search.php?s');
    return response.data.meals;
}

export async function fetchOneRecette(idMeal:number) {
    const response = await axios.get<{meals:Recette}>('https://www.themealdb.com/api/json/v1/1/lookup.php?i='+idMeal);
    return response.data.meals;
}

export async function fetchAllCategory() {
    const response = await axios.get<{categories:Category[]}>('https://www.themealdb.com/api/json/v1/1/categories.php');
    return response.data.categories;
}

export async function fetchOneCategory(strCategory: string): Promise<Category[]> {
    const response = await axios.get<Category[]>( `https://www.themealdb.com/api/json/v1/1/filter.php?c=${strCategory}`);
    return response.data;
  }
  

  export const fetchRecipesByCategory = async (category:any) => {
    const response = await fetch(
      `https://www.themealdb.com/api/json/v1/1/filter.php?c=${category}`
    );
    const data = await response.json();
  
    // Using Promise.all to fetch details of all recipes parallelly
    const recipeDetailsPromises = data.meals.map((meal:any) => {
      return fetch(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${meal.idMeal}`);
    });
  
    const recipeDetailsResponses = await Promise.all(recipeDetailsPromises);
    const recipeDetails = await Promise.all(recipeDetailsResponses.map((response) => response.json()));
  
    return recipeDetails.map((recipe) => recipe.meals[0]);
  };